require("dotenv").config();

const express = require("express");
const serveStatic = require("serve-static");
const path = require("path");
const jwt = require("express-jwt");
const jwtAuthz = require("express-jwt-authz");
const jwksRsa = require("jwks-rsa");

// Create a new Express app
const app = express();

// Set up Auth0 configuration
const authConfig = {
  domain: process.env.VUE_APP_DOMAIN,
  audience: process.env.VUE_APP_AUDIENCE
};

// Define middleware that validates incoming bearer tokens
// using JWKS from dev-hiycnple.au.auth0.com
const checkJwt = jwt({
  secret: jwksRsa.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://${authConfig.domain}/.well-known/jwks.json`
  }),

  audience: authConfig.audience,
  issuer: `https://${authConfig.domain}/`,
  algorithms: ["RS256"]
});

const checkPermissions = jwtAuthz(["admin:all"], {
  customScopeKey: "permissions",
  failWithError: true
});

// Define an endpoint that must be called with an access token
app.get("/api/external", checkJwt, (req, res) => {
  res.send({
    msg: "Your Access Token was successfully validated!"
  });
});

// Define an endpoint that must be called with an access token
app.get("/api/admin_only", checkJwt, checkPermissions, (req, res) => {
  res.send({
    admin: true,
    msg: "You have administrator access!"
  });
});

// Express Global Error Handler
app.use((err, req, res, next) => {
  if (err && err.statusCode && err.statusCode >= 400 && err.statusCode < 500) {
    res.status(401).send({
      admin: false,
      msg: "Unauthorised"
    });
  }

  next();
});

if (process.env.NODE_ENV !== "development") {
  app.use(serveStatic(path.join(__dirname, "dist")));
}

// Start the app
const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`API listening on ${port}`));
