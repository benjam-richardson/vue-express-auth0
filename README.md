# vue-express-auth0

To run the project, you need to create a `.env` file from the `.env.sample`


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
To run the API and Vue
```
npm run dev
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```
