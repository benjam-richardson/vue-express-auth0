const mapSettings = {
  clickableIcons: false,
  streetViewControl: false,
  panControlOptions: false,
  gestureHandling: "cooperative",
  mapTypeControl: false,
  zoomControlOptions: {
    style: "SMALL"
  },
  zoom: 12,
  minZoom: 5,
  maxZoom: 15
};

export { mapSettings };
